#!/bin/bash

DOWNLOAD_DIR=external/tarballs
URL=http://git.freescale.com/git/cgit.cgi/imx/linux-imx.git/snapshot/linux-imx-rel_imx_4.9.x_1.0.0_ga.tar.gz
LOCAL_NAME=linux-imx-rel_imx_4.9.x_1.0.0_ga.tar.gz

mkdir -p $DOWNLOAD_DIR
curl $URL -o $DOWNLOAD_DIR/$LOCAL_NAME
