################################################################################
#
# pipetest
#
################################################################################

PIPETEST_VERSION = 5.0.2
PIPETEST_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/pipetest
PIPETEST_SITE_METHOD = local
PIPETEST_SUPPORTS_IN_SOURCE_BUILD = NO
PIPETEST_INSTALL_STAGING = NO
PIPETEST_INSTALL_TARGET = YES
PIPETEST_INSTALL_IMAGES = NO

PIPETEST_DEPENDENCIES += opal-kelly-frontpanel

ifeq ($(BR2_ENABLE_DEBUG),y)
    PIPETEST_CONF_OPTS += -DCMAKE_BUILD_TYPE=DEBUG
else
    PIPETEST_CONF_OPTS += -DCMAKE_BUILD_TYPE=RELEASE
endif

PIPETEST_CONF_OPTS += --no-warn-unused-cli

define PIPETEST_INSTALL_TARGET_CMDS
    cp $(@D)/buildroot-build/pipetest $(TARGET_DIR)/bin
endef

$(eval $(cmake-package))
