################################################################################
#
# quadapp
#
################################################################################

QUADAPP_VERSION = 0.0.1
QUADAPP_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/quadapp
QUADAPP_SITE_METHOD = local
QUADAPP_SUPPORTS_IN_SOURCE_BUILD = NO
QUADAPP_INSTALL_STAGING = NO
QUADAPP_INSTALL_TARGET = YES
QUADAPP_INSTALL_IMAGES = NO

QUADAPP_DEPENDENCIES += opal-kelly-frontpanel

ifeq ($(BR2_ENABLE_DEBUG),y)
    QUADAPP_CONF_OPTS += -DCMAKE_BUILD_TYPE=DEBUG
else
    QUADAPP_CONF_OPTS += -DCMAKE_BUILD_TYPE=RELEASE
endif

QUADAPP_CONF_OPTS += --no-warn-unused-cli

define QUADAPP_INSTALL_TARGET_CMDS
    cp $(@D)/buildroot-build/quadapp $(TARGET_DIR)/bin
endef

$(eval $(cmake-package))
