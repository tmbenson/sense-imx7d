#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <chrono>

#include "okFrontPanelDLL.h"

OpalKelly::FrontPanelPtr initializeFPGA() {
    auto dev = OpalKelly::FrontPanelDevices().Open();
    if (!dev.get()) {
        printf("Device could not be opened.  Is one connected?\n");
        return dev;
    }

    printf("Found a device: %s\n", dev->GetBoardModelString(dev->GetBoardModel()).c_str());
    
    dev->LoadDefaultPLLConfiguration();	
    
    // Get some general information about the XEM.
    std::string str;
    printf("Device firmware version: %d.%d\n", dev->GetDeviceMajorVersion(), dev->GetDeviceMinorVersion());
    str = dev->GetSerialNumber();
    printf("Device serial number: %s\n", str.c_str());
    str = dev->GetDeviceID();
    printf("Device device ID: %s\n", str.c_str());
    
    // Download the configuration file.
    if (okCFrontPanel::NoError != dev->ConfigureFPGA("quadapp.bit")) {
        printf("FPGA configuration failed.\n");
        dev.reset();
        return(dev);
    }
    
    // Check for FrontPanel support in the FPGA configuration.
    if (dev->IsFrontPanelEnabled())
        printf("FrontPanel support is enabled.\n");
    else
        printf("FrontPanel support is not enabled.\n");
    
    return(dev);
}

uint32_t getFill(OpalKelly::FrontPanelPtr &xem) {
    xem->UpdateWireOuts();
    uint32_t v1 = xem->GetWireOutValue(0x20);
    uint32_t v2 = xem->GetWireOutValue(0x21);
    return (v1 << 16) | v2; 
}

void printFill(OpalKelly::FrontPanelPtr &xem) {
    xem->UpdateWireOuts();
    printf("Fill Level %04x%04x\n",xem->GetWireOutValue(0x20),xem->GetWireOutValue(0x21));
}

const int READSIZE = 1024*1024;
static unsigned char *buf = nullptr;
static FILE *fp = nullptr;

void doRead(OpalKelly::FrontPanelPtr &xem) {
    unsigned long readlen = READSIZE;
    if (!buf)
        buf = new unsigned char[readlen];
    if (!fp)
        fp = fopen("dump.dat","wb");
    auto ret = xem->ReadFromPipeOut(0xA0, readlen, buf);
    fwrite(buf,readlen,1,fp);
    /*    unsigned short *ptr = (unsigned short *)(buf);
    for (int i=0;i<readlen/2;i++) {
        if (ptr[i] != ((unsigned short) i)) {
            std::cout << "ERR! : ptr[" << i << "] = " << ptr[i] << " vs " << ((unsigned short) i) << "\n";
        }
        }*/
}
    

int main(int argc, char *argv[])
{
    bool decrypt;
    char infilename[128];
    char outfilename[128];
    char keystr[32];
    unsigned char key[8];
    int i, j;

    using namespace std::chrono;
    
    printf("---- Opal Kelly ---- FPGA-XFERCOUNTS Application v1.0 ----\n");
    printf("FrontPanel DLL loaded. Version: %s\n", okFrontPanelDLL_GetAPIVersionString());

    auto xem = initializeFPGA();

    // Send a reset signal

    printFill(xem);
    
    printf("Sending reset signal\n");
    xem->SetWireInValue(0x00, 0xff, 0x01);
    xem->UpdateWireIns();
    usleep(1000);
    xem->SetWireInValue(0x00, 0x00, 0x01);
    xem->UpdateWireIns();

    printFill(xem);
    // Tell the DRAM controller to write data to the DRAM

    printf("Enabling write on DRAM controller");
    xem->SetWireInValue(0x00, 0xff, 0x08);
    printFill(xem);

    printf("Sending run signal %d\n", i);
    xem->SetWireInValue(0x00, 0xff, 0x02);
    xem->UpdateWireIns();
    
    //  Send a run signal

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    int64_t bytes = 0;
    while(1) {
        int fill = getFill(xem);
        if (fill > READSIZE) {
            //            printf("Fill exceeds 1MB, trigger read\n");
            xem->ActivateTriggerIn(0x40,0);
            doRead(xem);
            bytes += READSIZE;
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(t2-t1);
            double MB = bytes/(1024.0*1024.0);
            printf("Rate is %g MB/sec, %g MB read, %d fill\r",MB/time_span.count(),MB,fill);
            fflush(stdout);
        } else {
            //            printFill(xem);
            usleep(50000);
        }
    }

    //    xem->SetWireInValue(0x00, 0x00, 0x08);
    //    usleep(1000);

    /*    
    xem->SetWireInValue(0x00, 0xff, 0x04);
    xem->UpdateWireIns();
    */

        
    // Send a trigger to the FPGA
    //    xem->ActivateTriggerIn(0x40, 3);

    
    return 0;
}
