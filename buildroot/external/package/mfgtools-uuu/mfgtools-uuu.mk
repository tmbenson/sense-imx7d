################################################################################
#
## mfgtools-uuu
#
#################################################################################

MFGTOOLS_UUU_VERSION = 1.1.41
MFGTOOLS_UUU_SOURCE = mfgtools-uuu_$(MFGTOOLS_UUU_VERSION)-patched.tar.gz
MFGTOOLS_UUU_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/mfgtools-uuu
MFGTOOLS_UUU_SITE_METHOD = file
MFGTOOLS_UUU_SUPPORTS_IN_SOURCE_BUILD = NO
MFGTOOLS_UUU_INSTALL_HOST = YES
MFGTOOLS_UUU_INSTALL_STAGING = NO
MFGTOOLS_UUU_INSTALL_TARGET = NO
MFGTOOLS_UUU_INSTALL_IMAGES = NO
MFGTOOLS_UUU_LICENSE = BSD-3-Clause
MFGTOOLS_UUU_LICENSE_FILES = LICENSE

HOST_MFGTOOLS_UUU_DEPENDENCIES = host-libusb

define HOST_MFGTOOLS_UUU_INSTALL_CMDS
  $(INSTALL) -D -m 0755 $(@D)/buildroot-build/uuu/uuu $(HOST_DIR)/bin/
endef

$(eval $(host-cmake-package))
