################################################################################
#
# opal-kelly-frontpanel
#
################################################################################

OPAL_KELLY_FRONTPANEL_VERSION = 5.0.2
OPAL_KELLY_FRONTPANEL_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/opal-kelly-frontpanel
OPAL_KELLY_FRONTPANEL_SITE_METHOD = local
OPAL_KELLY_FRONTPANEL_SUPPORTS_IN_SOURCE_BUILD = NO
OPAL_KELLY_FRONTPANEL_INSTALL_STAGING = YES
OPAL_KELLY_FRONTPANEL_INSTALL_TARGET = YES
OPAL_KELLY_FRONTPANEL_INSTALL_IMAGES = NO

OPAL_KELLY_FRONTPANEL_DEPENDENCIES = boost

define OPAL_KELLY_FRONTPANEL_INSTALL_STAGING_CMDS
    cp $(@D)/third-party/*.h $(STAGING_DIR)/usr/include
    cp $(@D)/third-party/*.so $(STAGING_DIR)/usr/lib
endef

define OPAL_KELLY_FRONTPANEL_INSTALL_TARGET_CMDS
    cp $(@D)/third-party/*.so $(TARGET_DIR)/usr/lib
    mkdir -p $(TARGET_DIR)/opt/opal-kelly-bitfiles/XEM6010-LX45
    cp $(@D)/third-party/XEM6010-LX45/*.bit  $(TARGET_DIR)/opt/opal-kelly-bitfiles/XEM6010-LX45
    # The OK libraries expect Boost version 1.62, but buildroot builds 1.67. They should
    # be compatible for these purposes.
    ln -sfr $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_system.so.1.67.0 $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_system.so.1.62.0
    ln -sfr $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_thread.so.1.67.0 $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_thread.so.1.62.0
    ln -sfr $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_chrono.so.1.67.0 $(HOST_DIR)/arm-buildroot-linux-gnueabihf/sysroot/usr/lib/libboost_chrono.so.1.62.0
    ln -sfr $(TARGET_DIR)/usr/lib/libboost_system.so.1.67.0 $(TARGET_DIR)/usr/lib/libboost_system.so.1.62.0
    ln -sfr $(TARGET_DIR)/usr/lib/libboost_thread.so.1.67.0 $(TARGET_DIR)/usr/lib/libboost_thread.so.1.62.0
    ln -sfr $(TARGET_DIR)/usr/lib/libboost_chrono.so.1.67.0 $(TARGET_DIR)/usr/lib/libboost_chrono.so.1.62.0
endef

$(eval $(generic-package))
