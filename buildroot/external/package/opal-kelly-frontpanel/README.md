The FrontPanel API is a proprietary library from Opal Kelly used to
interoperate with USB-connected Opal Kelly hardware. There is no generic
installer available, but there is a Raspbian (Debian on Raspberry Pi)
installer. This package takes the relevant files from the Raspberry Pi
installer and copies them to the staging and target directories.
