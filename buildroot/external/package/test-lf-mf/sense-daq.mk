################################################################################
#
# sense-daq
#
################################################################################

SENSE_DAQ_VERSION = 0.0.1
SENSE_DAQ_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/sense-daq
SENSE_DAQ_SITE_METHOD = local
SENSE_DAQ_SUPPORTS_IN_SOURCE_BUILD = NO
SENSE_DAQ_INSTALL_STAGING = NO
SENSE_DAQ_INSTALL_TARGET = YES
SENSE_DAQ_INSTALL_IMAGES = NO

SENSE_DAQ_DEPENDENCIES += opal-kelly-frontpanel

ifeq ($(BR2_ENABLE_DEBUG),y)
    SENSE_DAQ_CONF_OPTS += -DCMAKE_BUILD_TYPE=DEBUG
else
    SENSE_DAQ_CONF_OPTS += -DCMAKE_BUILD_TYPE=RELEASE
endif

SENSE_DAQ_CONF_OPTS += --no-warn-unused-cli

define SENSE_DAQ_INSTALL_TARGET_CMDS
    cp $(@D)/buildroot-build/sense_daq $(TARGET_DIR)/bin
endef

$(eval $(cmake-package))
