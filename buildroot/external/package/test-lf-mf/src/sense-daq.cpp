#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <chrono>

#include "okFrontPanelDLL.h"

OpalKelly::FrontPanelPtr initializeFPGA() {
    auto dev = OpalKelly::FrontPanelDevices().Open();
    if (!dev.get()) {
        printf("Device could not be opened.  Is one connected?\n");
        return dev;
    }

    printf("Found a device: %s\n", dev->GetBoardModelString(dev->GetBoardModel()).c_str());
    
    dev->LoadDefaultPLLConfiguration();	
    
    // Get some general information about the XEM.
    std::string str;
    printf("Device firmware version: %d.%d\n", dev->GetDeviceMajorVersion(), dev->GetDeviceMinorVersion());
    str = dev->GetSerialNumber();
    printf("Device serial number: %s\n", str.c_str());
    str = dev->GetDeviceID();
    printf("Device device ID: %s\n", str.c_str());
    
    // Download the configuration file.
    if (okCFrontPanel::NoError != dev->ConfigureFPGA("spi_test.bit")) {
        printf("FPGA configuration failed.\n");
        dev.reset();
        return(dev);
    }
    
    // Check for FrontPanel support in the FPGA configuration.
    if (dev->IsFrontPanelEnabled())
        printf("FrontPanel support is enabled.\n");
    else
        printf("FrontPanel support is not enabled.\n");
    
    return(dev);
}

bool spi_txn(OpalKelly::FrontPanelPtr &xem, uint64_t mosi, uint64_t &miso, uint8_t bits, int numtries) {
    xem->SetWireInValue(0x0, (mosi >> 48) & 0xFFFF);
    xem->SetWireInValue(0x1, (mosi >> 32) & 0xFFFF);
    xem->SetWireInValue(0x2, (mosi >> 16) & 0xFFFF);
    xem->SetWireInValue(0x3, (mosi >> 0) & 0xFFFF);
    xem->SetWireInValue(0x4, bits);
    xem->UpdateWireIns();
    xem->ActivateTriggerIn(0x40, 0);
    bool ok = false;
    for (int i=0;i<numtries;i++) {
        xem->UpdateTriggerOuts();
        usleep(1000);
        if (xem->IsTriggered(0x60, 0x01) == true) {
            ok = true;
            break;
        }
    }
    if (!ok) return false;
    xem->UpdateWireOuts();
    uint64_t w0 = xem->GetWireOutValue(0x20);
    uint64_t w1 = xem->GetWireOutValue(0x21);
    uint64_t w2 = xem->GetWireOutValue(0x22);
    uint64_t w3 = xem->GetWireOutValue(0x23);
    miso = 0;
    miso = (w0 << 48) | (w1 << 32) | (w2 << 16) | (w3 << 0);
    return true;
}


struct adc_reg {
    std::string name;
    uint8_t addr;
    uint8_t width;
};

const adc_reg adc7193_status{"status", 0, 8};
const adc_reg adc7193_mode{"mode",     1, 24};
const adc_reg adc7193_config{"config", 2, 24};
const adc_reg adc7193_data{"data",     3, 24};
const adc_reg adc7193_id{"ID",         4, 8};
const adc_reg adc7193_gpocon{"GPOcon", 5, 8};
const adc_reg adc7193_offset{"offset", 6, 24};
const adc_reg adc7193_fscale{"fscale", 7, 24};

const std::vector<adc_reg> adc7193_regs = {adc7193_status, adc7193_mode, adc7193_config,
                                           adc7193_data, adc7193_id, adc7193_gpocon,
                                           adc7193_offset, adc7193_fscale};

const adc_reg adc7770_ch_disable{"ch_disable", 0x08, 8};
const adc_reg adc7770_general_user_config_1{"general_user_config_1", 0x11, 8};
const adc_reg adc7770_general_user_config_2{"general_user_config_2", 0x12, 8};
const adc_reg adc7770_general_user_config_3{"general_user_config_3", 0x13, 8};
const adc_reg adc7770_dout_format{"dout_format", 0x14, 8};
const adc_reg adc7770_gen_err_reg_1{"gen_err_reg_1", 0x59, 8};
const adc_reg adc7770_gen_err_reg_2{"gen_err_reg_2", 0x5b, 8};
const adc_reg adc7770_status_reg_1{"status_reg_1", 0x5d, 8};
const adc_reg adc7770_status_reg_2{"status_reg_2", 0x5e, 8};
const adc_reg adc7770_status_reg_3{"status_reg_3", 0x5f, 8};
const adc_reg adc7770_src_n_msb{"src_n_msb", 0x60, 8};
const adc_reg adc7770_src_n_lsb{"src_n_lsb", 0x61, 8};
const adc_reg adc7770_src_if_msb{"src_if_msb", 0x62, 8};
const adc_reg adc7770_src_if_lsb{"src_if_lsb", 0x63, 8};
const adc_reg adc7770_src_update{"src_update", 0x64, 8};

const std::vector<adc_reg> adc7770_regs = {adc7770_ch_disable,
                                           adc7770_general_user_config_1,
                                           adc7770_general_user_config_2,
                                           adc7770_general_user_config_3,
                                           adc7770_dout_format,
                                           adc7770_gen_err_reg_1,
                                           adc7770_gen_err_reg_2,
                                           adc7770_status_reg_1,
                                           adc7770_status_reg_2,
                                           adc7770_status_reg_3,
                                           adc7770_src_n_msb,
                                           adc7770_src_n_lsb,
                                           adc7770_src_if_msb,
                                           adc7770_src_if_lsb,
                                           adc7770_src_update};

const uint8_t READCMD_7193 = 0x40;
const uint8_t READCMD_7770 = 0x80;

void select_chip(OpalKelly::FrontPanelPtr &xem, bool select_1414, int bank) {
    uint8_t val = ((select_1414 ? 0 : 1) << 2) | bank;
    printf("Select set to %x\n",val);
    xem->SetWireInValue(0x5, val);
    xem->UpdateWireIns();
}

bool print_7193_reg(OpalKelly::FrontPanelPtr &xem, const adc_reg &reg) {
    uint64_t mosi = (reg.addr << 3) | READCMD_7193;
    mosi = (mosi << reg.width);
    uint64_t miso = 0;
    auto ok = spi_txn(xem, mosi, miso, 8 + reg.width, 100);
    if (!ok) return false;
    uint32_t regval = uint32_t(miso & 0xFFFFFFFFull);
    printf("Register %s : %x\n", reg.name.c_str(), regval);
    return true;
}

bool print_7770_reg(OpalKelly::FrontPanelPtr &xem, const adc_reg &reg) {
    uint64_t mosi = reg.addr | READCMD_7770;
    mosi = (mosi << reg.width);
    uint64_t miso = 0;
    auto ok = spi_txn(xem, mosi, miso, 8 + reg.width, 100);
    if (!ok) return false;
    uint32_t regval = uint32_t(miso & 0xFFFFFFFFull);
    printf("Register %s : %x\n", reg.name.c_str(), regval);
    return true;
}

int main(int argc, char *argv[])
{
    using namespace std::chrono;
    
    printf("---- Opal Kelly ---- FPGA-XFERCOUNTS Application v1.0 ----\n");
    printf("FrontPanel DLL loaded. Version: %s\n", okFrontPanelDLL_GetAPIVersionString());

    auto xem = initializeFPGA();

    // Set up a transaction to send a 64 bit spi transaction
    
    // Send the trigger

    uint64_t miso = 0;
    bool ok;
    for (int bank=0;bank<4;bank++) {
        printf("Bank %d\n", bank);
        select_chip(xem, true, bank);
        spi_txn(xem, 0x0A, miso, 8, 100);
        select_chip(xem, false, bank);
        // Send a reset
        ok = spi_txn(xem, 0xFFFFFFFFFFFFFFFFull, miso, 64, 100);
        printf("Reset : %d\n", ok);
        sleep(1);
        for (const auto &p : adc7193_regs)   print_7193_reg(xem, p);
    }

//    
//    // setup the select lines
//    int select = 0;
//    xem->SetWireInValue(0x5, select);
//    xem->UpdateWireIns();
//    auto ok = spi_txn(xem, 0x0ull, miso, 64, 100);
//
//    select = 0x4; // enable ADC7193 #0
//    xem->SetWireInValue(0x5, select);
//    xem->UpdateWireIns();
//
//  
//
//    select = 0x5; // enable ADC7193 #1
//    xem->SetWireInValue(0x5, select);
//    xem->UpdateWireIns();
//
//    // Send a reset
//    ok = spi_txn(xem, 0xFFFFFFFFFFFFFFFFull, miso, 64, 100);
//    printf("Reset : %d\n", ok);
//    sleep(1);
//
//    for (const auto &p : adc7193_regs)   print_7193_reg(xem, p);
//
//    select = 0x6; // enable ADC7193 #2
//    xem->SetWireInValue(0x5, select);
//    xem->UpdateWireIns();
//
//    // Send a reset
//    ok = spi_txn(xem, 0xFFFFFFFFFFFFFFFFull, miso, 64, 100);
//    printf("Reset : %d\n", ok);
//    sleep(1);
//
//    for (const auto &p : adc7193_regs)   print_7193_reg(xem, p);
//
//    select = 0x7; // enable ADC7193 #2
//    xem->SetWireInValue(0x5, select);
//    xem->UpdateWireIns();
//
//    // Send a reset
//    ok = spi_txn(xem, 0xFFFFFFFFFFFFFFFFull, miso, 64, 100);
//    printf("Reset : %d\n", ok);
//    sleep(1);
//
//    for (const auto &p : adc7193_regs)   print_7193_reg(xem, p);
//
//    return 0;
//    int counter = 0;
//    // Send an ID request
    // We want to send a read command to the ADC
    // This is 01xx x000
    // The ID byte is 100, so
    //         0110 0000, or 0x60
    // The reply is 8 bits as well

    

   
    //    spi_txn(xem, 0x50, miso, 8, 100);
    
    /*    while (1) {
        uint64_t reg_0xdd = 0;
        spi_txn(xem, 0xDD00ull, reg_0xdd, 16, 100);
        printf("Register read %d\n", uint8_t(reg_0xdd));
        usleep(100);
        }*/

    /*
    for (const auto &p : adc7770_regs) {
        print_7770_reg(xem, p);
        }*/
    
    return 0;
}
