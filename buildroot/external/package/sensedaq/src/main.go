package main

// #cgo linux,amd64 CPPFLAGS: -I/opt/FrontPanel-Ubuntu16.04LTS-x64-5.0.2/API
// #cgo linux,amd64 CXXFLAGS: -std=gnu++14 -O3 -Wall -Werror
// #cgo linux,amd64 LDFLAGS: -L/opt/FrontPanel-Ubuntu16.04LTS-x64-5.0.2/API -L. -ldaq -lokFrontPanel
// #cgo linux,arm LDFLAGS: -L. -ldaq -lokFrontPanel
// #include "libdaq.h"
import "C"

import (
	"fmt"
	"time"
)

func main() {
	rc := C.LibDaq_InitializeDevices()
	fmt.Printf("InitalizeDevices() = %d\n", int(rc))
	fmt.Printf("NumDevices = %d\n", uint(C.LibDaq_GetNumDevices()))
	bitfile := C.CString("quadapp.bit")
	rc = C.LibDaq_LoadFirmware(C.CString("1744000KA6"), bitfile)
	if rc != 0 {
		fmt.Printf("Failed to load firmware\n")
	}
	C.LibDaq_StartCollection()
	time.Sleep(10 * time.Second)
	fmt.Printf("Calling StopCollection\n")
	C.LibDaq_StopCollection()
	C.LibDaq_Reset()
}
