#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <cinttypes>
#include <chrono>
#include <cmath>
#include <memory>
#include <map>
#include <string>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <cassert>
#include <algorithm>
#include <pthread.h>
#include <sched.h>

#include "okFrontPanelDLL.h"

#include "libdaq.h"

#define RMS_NUM_WINDOW_BUFS (256)
#define RMS_WINDOW_LEN_MS (20)
#define RMS_WINDOW_LEN_NELEM (RMS_WINDOW_LEN_MS * 1000)
#define RMS_MAX_DERIV_ORDER (5)

#define HF_NUM_CHANNELS (4)
#define HF_BYTES_PER_SAMPLE (2)
#define HF_SAMPLES_PER_SEC_PER_CHAN (1000000)

//#define INJECT_TEST_PATTERN

using OpalKellyLegacy::okCFrontPanel;

enum class CollectionState {
    NotRunning,
    Stopping,
    Running
};

static struct {
    // Working buffers for the current RMS window
    float rmsAccum[RMS_MAX_DERIV_ORDER+1][HF_NUM_CHANNELS];
    float rmsWorkBufs[RMS_MAX_DERIV_ORDER+1][HF_NUM_CHANNELS][RMS_WINDOW_LEN_NELEM];
    // Stored RMS values and data
    float rmsValues[RMS_NUM_WINDOW_BUFS][HF_NUM_CHANNELS][RMS_MAX_DERIV_ORDER+1];
    uint16_t data[RMS_NUM_WINDOW_BUFS][HF_NUM_CHANNELS][RMS_WINDOW_LEN_NELEM];
} s_hfRing;

static struct HfMeta {
    std::mutex lock;
    std::condition_variable cv;

    int64_t numBuffersFilled { 0 };
    int64_t numBuffersProcessed { 0 };
    int32_t fillBufInd { 0 };
    int32_t numSamplesInFillBuf { 0 };
    int32_t numOverruns { 0 };
    uint32_t okPipeMaxFill { 0 };
    int32_t numBuffersFreeLowWatermark { RMS_NUM_WINDOW_BUFS };

    int32_t numReads { 0 };
    double ingestUsecs { 0.0 };
    double pipeReadUsecs { 0.0 };
#ifdef INJECT_TEST_PATTERN
    uint16_t nextTestPattern { 0 };
#endif // INJECT_TEST_PATTERN
} s_hfMeta;

static struct {
    std::map<std::string, std::unique_ptr<okCFrontPanel>> devices;
    std::mutex mutex;
    std::thread hfReader;
    CollectionState collectionState;
} s_state;

long LibDaq_InitializeDevices()
{
    s_state.collectionState = CollectionState::NotRunning;
    OpalKelly::FrontPanelDevices devices;
    const int numDevices = devices.GetCount();
    printf("Number of devices: %d\n", numDevices);
    long rc = 0;
    for (int i = 0; i < numDevices; i++) {
        const std::string serial = devices.GetSerial(i);
        printf("Device %d has serial %s\n", i, serial.c_str());
        auto ptr = devices.Open(serial);
        if (ptr) {
            printf("Opened device with serial %s\n", serial.c_str());
            printf("\tDevice model: %s\n", ptr->GetBoardModelString(ptr->GetBoardModel()).c_str());
            printf("\tDevice firmware version: %d.%d\n",
                ptr->GetDeviceMajorVersion(), ptr->GetDeviceMinorVersion());
            printf("\tDevice device ID: %s\n", ptr->GetDeviceID().c_str());
            s_state.devices[serial] = std::move(ptr);
        } else {
            printf("Failed to open device with serial %s\n", serial.c_str());
            rc |= 1;
        }
    }
    return rc;
}

long LibDaq_LoadFirmware(const char *serialNumber, const char *bitfile)
{
    const std::string serial(serialNumber);
    if (! s_state.devices.count(serial)) {
        printf("Device with serial number %s not open; cannot load firmware\n",
            serialNumber);
        return okCFrontPanel::DeviceNotOpen;
    }

    s_state.devices[serial]->LoadDefaultPLLConfiguration();
    auto rc = s_state.devices[serial]->ConfigureFPGA(bitfile);
    if (rc != okCFrontPanel::NoError) {
        printf("Failed to load firmware to device %s: %s\n",
            serialNumber, LibDaq_GetErrorString(rc));
        return rc;
    }

    printf("Loaded %s to device %s\n", bitfile, serialNumber);
    return okCFrontPanel::NoError;
}

size_t LibDaq_GetNumDevices()
{
    return s_state.devices.size();
}

void LibDaq_Reset()
{
    for (auto & dev : s_state.devices) {
        if (dev.second) {
            dev.second->Close();
            dev.second.reset();
        }
    }
    s_state.devices.clear();
}

const char *LibDaq_GetErrorString(long err)
{
    switch (err) {
        case okCFrontPanel::NoError: return "no error";
        case okCFrontPanel::Failed: return "generic failure";
        case okCFrontPanel::Timeout: return "timeout";
        case okCFrontPanel::DoneNotHigh: return "done not high";
        case okCFrontPanel::TransferError: return "transfer error";
        case okCFrontPanel::CommunicationError: return "communication error";
        case okCFrontPanel::InvalidBitstream: return "invalid bitstream";
        case okCFrontPanel::FileError: return "file error";
        case okCFrontPanel::DeviceNotOpen: return "device not open";
        case okCFrontPanel::InvalidEndpoint: return "invalid endpoint";
        case okCFrontPanel::InvalidBlockSize: return "invalid block size";
        case okCFrontPanel::I2CRestrictedAddress: return "I2C restricted address";
        case okCFrontPanel::I2CBitError: return "I2C bit error";
        case okCFrontPanel::I2CNack: return "I2C nack";
        case okCFrontPanel::I2CUnknownStatus: return "I2C unknown status";
        case okCFrontPanel::UnsupportedFeature: return "unsupported feature";
        case okCFrontPanel::FIFOUnderflow: return "FIFO underflow";
        case okCFrontPanel::FIFOOverflow: return "FIFO overflow";
        case okCFrontPanel::DataAlignmentError: return "data alignment error";
        case okCFrontPanel::InvalidResetProfile: return "invalid reset profile";
        case okCFrontPanel::InvalidParameter: return "invalid parameter";
        default: return "unknown error";
    }
}

uint32_t getFill(okCFrontPanel *xem) {
    xem->UpdateWireOuts();
    uint32_t v1 = xem->GetWireOutValue(0x20);
    uint32_t v2 = xem->GetWireOutValue(0x21);
    return (v1 << 16) | v2;
}

#define HF_BLOCK_SIZE_BYTES (1024*1024)
#define HF_CACHE_AWARE_COPY_SIZE_BYTES (4*1024)
#define HF_CACHE_AWARE_COPY_NUM_ELEM (HF_CACHE_AWARE_COPY_SIZE_BYTES/sizeof(uint16_t)/HF_NUM_CHANNELS)

static void IngestHfBlock(const uint16_t *input, int32_t numSamples)
{
    const int32_t bufInd = s_hfMeta.fillBufInd;
    const int32_t startInd = s_hfMeta.numSamplesInFillBuf;
    const int32_t stopInd = startInd + numSamples;

    uint16_t *data[HF_NUM_CHANNELS];
    for (int c = 0; c < HF_NUM_CHANNELS; c++) {
        data[c] = s_hfRing.data[bufInd][c];
    }

    assert(stopInd <= RMS_WINDOW_LEN_NELEM);

    // Compile with -O3 -funroll-loops to unroll the inner loop.
    int32_t srcInd = 0;
    for (int i = startInd; i < stopInd; i++) {
        for (int c = 0; c < HF_NUM_CHANNELS; c++) {
            data[c][i] = input[srcInd++];
        }
    }

    for (int32_t i = startInd; i < stopInd; i++) {
        for (int c = 0; c < HF_NUM_CHANNELS; c++) {
            const float sample = static_cast<float>(data[c][i]);
            s_hfRing.rmsWorkBufs[0][c][i] = sample;
            s_hfRing.rmsAccum[0][c] += sample * sample;
        }
    }

    for (int32_t d = 1; d <= RMS_MAX_DERIV_ORDER; d++) {
        for (int32_t i = std::max((int32_t)startInd-d, 0); i < stopInd-d; i++) {
            for (int c = 0; c < HF_NUM_CHANNELS; c++) {
                s_hfRing.rmsWorkBufs[d][c][i] = s_hfRing.rmsWorkBufs[d-1][c][i+1] - s_hfRing.rmsWorkBufs[d-1][c][i];
                s_hfRing.rmsAccum[d][c] += s_hfRing.rmsWorkBufs[d][c][i] * s_hfRing.rmsWorkBufs[d][c][i];
            }
        }
    }

    if (stopInd == RMS_WINDOW_LEN_NELEM) {
        for (int32_t d = 0; d <= RMS_MAX_DERIV_ORDER; d++) {
            for (int c = 0; c < HF_NUM_CHANNELS; c++) {
                s_hfRing.rmsValues[bufInd][c][d] = sqrtf(s_hfRing.rmsAccum[d][c]/(RMS_WINDOW_LEN_NELEM-d));
                s_hfRing.rmsAccum[d][c] = 0.0f;
            }
        }
    }
}

#ifdef INJECT_TEST_PATTERN
static void VerifyTestPatternBlock()
{
    const int32_t bufInd = s_hfMeta.fillBufInd;
    for (int i = 0; i < RMS_WINDOW_LEN_NELEM; i++) {
        for (int c = 0; c < HF_NUM_CHANNELS; c++) {
            if (s_hfRing.data[bufInd][c][i] != s_hfMeta.nextTestPattern) {
                printf("Test pattern failure: buf %d: expected %u, read %u\n",
                    bufInd, s_hfMeta.nextTestPattern, s_hfRing.data[bufInd][c][i]);
            }
        }
        s_hfMeta.nextTestPattern++;
    }
}
#endif // INJECT_TEST_PATTERN

static void IngestHfData(const uint8_t *raw, int32_t numBytes)
{
    //FIXME: For now, I assume that the first channel is index 0.
    //Eventually, the channel will be in the MSBs of the data.
    const uint16_t *data = reinterpret_cast<const uint16_t *>(raw);
    const int32_t numTotal = numBytes/static_cast<int32_t>(sizeof(uint16_t));
    const int32_t numPerChan = numTotal/HF_NUM_CHANNELS;
    int32_t numRemaining = numPerChan;
    const auto start = std::chrono::steady_clock::now();
    while (numRemaining > 0) {
        const int32_t numToIngest = std::min(
            std::min((int32_t)HF_CACHE_AWARE_COPY_NUM_ELEM, numRemaining),
            RMS_WINDOW_LEN_NELEM - (int32_t) s_hfMeta.numSamplesInFillBuf);
        IngestHfBlock(data, numToIngest);
        data += numToIngest * HF_NUM_CHANNELS;
        numRemaining -= numToIngest;
        s_hfMeta.numSamplesInFillBuf += numToIngest;
        if (s_hfMeta.numSamplesInFillBuf == RMS_WINDOW_LEN_NELEM) {
#ifdef INJECT_TEST_PATTERN
            VerifyTestPatternBlock();
#endif // INJECT_TEST_PATTERN
            s_hfMeta.numSamplesInFillBuf = 0;
            s_hfMeta.fillBufInd = (s_hfMeta.fillBufInd + 1) % RMS_NUM_WINDOW_BUFS;
            s_hfMeta.numBuffersFilled++;
            const int64_t numBuffersFree = RMS_NUM_WINDOW_BUFS - (s_hfMeta.numBuffersFilled -
                s_hfMeta.numBuffersProcessed);
            if (numBuffersFree < s_hfMeta.numBuffersFreeLowWatermark) {
                s_hfMeta.numBuffersFreeLowWatermark = numBuffersFree;
            }
            if (numBuffersFree <= 0) {
                s_hfMeta.numOverruns++;
            }
        }
    }
    const auto elapsedUs = std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::steady_clock::now() - start).count();
    s_hfMeta.ingestUsecs += static_cast<double>(elapsedUs);
}

static void hfReadLoop()
{
    if (! s_state.devices.size()) {
        printf("No opened devices; cannot read data\n");
        return;
    }

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(1, &cpuset);
    int rc = 0;
    if ((rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset)) != 0) {
        fprintf(stderr, "Unable to set HF reader CPU affinity to 1: rc=%d\n", rc);
        exit(EXIT_FAILURE);
    }

    okCFrontPanel *xem = nullptr;
    for (auto & dev : s_state.devices) {
        xem = dev.second.get();
        break;
    }

    std::unique_ptr<uint8_t[]> buf(new uint8_t[HF_BLOCK_SIZE_BYTES]);
    memset(buf.get(), 0, sizeof(uint8_t)*HF_BLOCK_SIZE_BYTES);

    printf("Sending reset signal\n");
    xem->SetWireInValue(0x00, 0xff, 0x01);
    xem->UpdateWireIns();
    usleep(1000);
    xem->SetWireInValue(0x00, 0x00, 0x01);
    xem->UpdateWireIns();

    printf("Enabling write on DRAM controller\n");
    xem->SetWireInValue(0x00, 0xff, 0x08);

    printf("Sending run signal\n");
    xem->SetWireInValue(0x00, 0xff, 0x02);
    xem->UpdateWireIns();

    s_state.mutex.lock();
    s_state.collectionState = CollectionState::Running;
    s_state.mutex.unlock();

    const double bytesPerMicrosecond = (HF_SAMPLES_PER_SEC_PER_CHAN * HF_BYTES_PER_SAMPLE *
        HF_NUM_CHANNELS) / 1.0e6;
    const auto start = std::chrono::steady_clock::now();
    int64_t bytes = 0;
#ifdef INJECT_TEST_PATTERN
    uint16_t testPatternNext = 0;
#endif // INJECT_TEST_PATTERN
    while(1) {
        s_state.mutex.lock();
        CollectionState collectionState = s_state.collectionState;
        s_state.mutex.unlock();

        if (collectionState == CollectionState::Stopping) {
            break;
        }

        const uint32_t fill = getFill(xem);
        const auto fillCheckTime = std::chrono::steady_clock::now();
        if (fill > s_hfMeta.okPipeMaxFill) {
            s_hfMeta.okPipeMaxFill = fill;
        }
        if (fill >= HF_BLOCK_SIZE_BYTES) {
            xem->ActivateTriggerIn(0x40,0);
            long ret = xem->ReadFromPipeOut(0xA0, HF_BLOCK_SIZE_BYTES, buf.get());
            s_hfMeta.pipeReadUsecs += static_cast<double>(
                std::chrono::duration_cast<std::chrono::microseconds>(
                    std::chrono::steady_clock::now() - fillCheckTime).count());
            if (ret < 0) {
                printf("Error reading from pipe: rc=%ld\n", ret);
                break;
            }
            if (ret != HF_BLOCK_SIZE_BYTES) {
                printf("Shorted read: expected %d bytes, read %ld\n",
                    HF_BLOCK_SIZE_BYTES, ret);
                break;
            }
#ifdef INJECT_TEST_PATTERN
            uint16_t * const ptr = reinterpret_cast<uint16_t *>(buf.get());
            for (int i = 0; i < (HF_BLOCK_SIZE_BYTES/2)/HF_NUM_CHANNELS; i++) {
                for (int c = 0; c < HF_NUM_CHANNELS; c++) {
                    ptr[i*HF_NUM_CHANNELS+c] = testPatternNext;
                }
                testPatternNext++;
            }
#endif // INJECT_TEST_PATTERN
            s_hfMeta.numReads++;
            IngestHfData(buf.get(), ret);
            bytes += ret;
            const auto elapsedMs = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now() - start).count();
            double MB = bytes/(1024.0*1024.0);
            printf("Rate is %g MB/sec, %g MB read, %u fill\r",MB/(elapsedMs/1.0e3),MB,fill);
            fflush(stdout);
        } else {
            const double usecsToSleep = (HF_BLOCK_SIZE_BYTES - (int32_t)fill)/
                bytesPerMicrosecond;
            usleep(usecsToSleep);
        }
    }

    printf("Clearing run signal\n");
    xem->SetWireInValue(0x00, 0x00, 0x02);
    xem->UpdateWireIns();

    printf("Total bytes read: %" PRId64 "\n", bytes);
    printf("Max pipe fill value: %u\n", s_hfMeta.okPipeMaxFill);
    printf("Number of %d ms buffers filled: %" PRId64 "\n",
        RMS_WINDOW_LEN_MS, s_hfMeta.numBuffersFilled);
    printf("NumBuffersFreeLowWatermark: %d\n", s_hfMeta.numBuffersFreeLowWatermark);
    printf("NumOverruns: %d\n", s_hfMeta.numOverruns);
    printf("Ingest time avg: %.3f us\n", s_hfMeta.ingestUsecs/s_hfMeta.numReads);
    printf("Read time avg: %.3f us\n", s_hfMeta.pipeReadUsecs/s_hfMeta.numReads);
}

long LibDaq_StartCollection()
{
    s_state.hfReader = std::thread(hfReadLoop);
    return 0;
}

void LibDaq_StopCollection()
{
    s_state.mutex.lock();
    if (s_state.collectionState == CollectionState::Running) {
        printf("Stopping collection\n");
        s_state.collectionState = CollectionState::Stopping;
    }
    s_state.mutex.unlock();

    if (s_state.hfReader.joinable()) {
        printf("Waiting on HF reader\n");
        s_state.hfReader.join();
    }

    s_state.mutex.lock();
    printf("Collection stopped\n");
    s_state.collectionState = CollectionState::NotRunning;
    s_state.mutex.unlock();
}

#if 0
OpalKelly::FrontPanelPtr initializeFPGA(const char *bitfile_name) {
    auto dev = OpalKelly::FrontPanelDevices().Open();
    if (!dev.get()) {
        printf("Device could not be opened.  Is one connected?\n");
        return dev;
    }

    printf("Found a device: %s\n", dev->GetBoardModelString(dev->GetBoardModel()).c_str());

    dev->LoadDefaultPLLConfiguration();

    // Get some general information about the XEM.
    std::string str;
    printf("Device firmware version: %d.%d\n", dev->GetDeviceMajorVersion(), dev->GetDeviceMinorVersion());
    str = dev->GetSerialNumber();
    printf("Device serial number: %s\n", str.c_str());
    str = dev->GetDeviceID();
    printf("Device device ID: %s\n", str.c_str());

    // Download the configuration file.
    if (okCFrontPanel::NoError != dev->ConfigureFPGA(bitfile_name)) {
        printf("FPGA configuration failed.\n");
        dev.reset();
        return(dev);
    }

    // Check for FrontPanel support in the FPGA configuration.
    if (dev->IsFrontPanelEnabled())
        printf("FrontPanel support is enabled.\n");
    else
        printf("FrontPanel support is not enabled.\n");

    return(dev);
}

uint32_t getFill(OpalKelly::FrontPanelPtr &xem) {
    xem->UpdateWireOuts();
    uint32_t v1 = xem->GetWireOutValue(0x20);
    uint32_t v2 = xem->GetWireOutValue(0x21);
    return (v1 << 16) | v2;
}

void printFill(OpalKelly::FrontPanelPtr &xem) {
    xem->UpdateWireOuts();
    printf("Fill Level %04x%04x\n",xem->GetWireOutValue(0x20),xem->GetWireOutValue(0x21));
}

const int READSIZE = 1024*1024;
static unsigned char *buf = nullptr;
static FILE *fp = nullptr;

void doRead(OpalKelly::FrontPanelPtr &xem) {
    unsigned long readlen = READSIZE;
    if (!buf)
        buf = new unsigned char[readlen];
    auto ret = xem->ReadFromPipeOut(0xA0, readlen, buf);
    /*    unsigned short *ptr = (unsigned short *)(buf);
    for (int i=0;i<readlen/2;i++) {
        if (ptr[i] != ((unsigned short) i)) {
            std::cout << "ERR! : ptr[" << i << "] = " << ptr[i] << " vs " << ((unsigned short) i) << "\n";
        }
        }*/
}



int main(int argc, char *argv[])
{
    bool decrypt;
    char infilename[128];
    char outfilename[128];
    char keystr[32];
    unsigned char key[8];
    int i, j;

    using namespace std::chrono;

    printf("---- Opal Kelly ---- FPGA-XFERCOUNTS Application v1.0 ----\n");
    printf("FrontPanel DLL loaded. Version: %s\n", okFrontPanelDLL_GetAPIVersionString());

    auto xem = initializeFPGA();

    // Send a reset signal

    printFill(xem);

    printf("Sending reset signal\n");
    xem->SetWireInValue(0x00, 0xff, 0x01);
    xem->UpdateWireIns();
    usleep(1000);
    xem->SetWireInValue(0x00, 0x00, 0x01);
    xem->UpdateWireIns();

    printFill(xem);
    // Tell the DRAM controller to write data to the DRAM

    printf("Enabling write on DRAM controller");
    xem->SetWireInValue(0x00, 0xff, 0x08);
    printFill(xem);

    printf("Sending run signal %d\n", i);
    xem->SetWireInValue(0x00, 0xff, 0x02);
    xem->UpdateWireIns();

    //  Send a run signal

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    int64_t bytes = 0;
    while(1) {
        int fill = getFill(xem);
        if (fill > READSIZE) {
            //            printf("Fill exceeds 1MB, trigger read\n");
            xem->ActivateTriggerIn(0x40,0);
            doRead(xem);
            bytes += READSIZE;
            high_resolution_clock::time_point t2 = high_resolution_clock::now();
            duration<double> time_span = duration_cast<duration<double>>(t2-t1);
            double MB = bytes/(1024.0*1024.0);
            printf("Rate is %g MB/sec, %g MB read, %d fill\r",MB/time_span.count(),MB,fill);
            fflush(stdout);
        } else {
            //            printFill(xem);
            usleep(50000);
        }
    }

    //    xem->SetWireInValue(0x00, 0x00, 0x08);
    //    usleep(1000);

    /*
    xem->SetWireInValue(0x00, 0xff, 0x04);
    xem->UpdateWireIns();
    */


    // Send a trigger to the FPGA
    //    xem->ActivateTriggerIn(0x40, 3);


    return 0;
}
#endif
