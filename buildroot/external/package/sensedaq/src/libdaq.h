#ifndef _LIBDAQ_H_
#define _LIBDAQ_H_

#ifdef __cplusplus
#include <cstddef>
extern "C" {
#else // ! __cplusplus
#include <stddef.h>
#endif // __cplusplus

long LibDaq_InitializeDevices();
size_t LibDaq_GetNumDevices();
long LibDaq_LoadFirmware(const char *serialNumber, const char *bitfile);
long LibDaq_StartCollection();
void LibDaq_StopCollection();
const char *LibDaq_GetErrorString(long err);
void LibDaq_Reset();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _LIBDAQ_H_
