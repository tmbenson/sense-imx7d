################################################################################
#
# sensedaq
#
################################################################################

SENSEDAQ_VERSION = 0.0.1
SENSEDAQ_SITE = $(BR2_EXTERNAL_SMITHS_DIGITAL_FORGE_PATH)/package/sensedaq
SENSEDAQ_SITE_METHOD = local
SENSEDAQ_SUPPORTS_IN_SOURCE_BUILD = NO
SENSEDAQ_INSTALL_STAGING = NO
SENSEDAQ_INSTALL_TARGET = YES
SENSEDAQ_INSTALL_IMAGES = NO

SENSEDAQ_DEPENDENCIES += opal-kelly-frontpanel host-go

SENSEDAQ_ENV += GOOS=linux GOARM=7 GOGCCFLAGS="-O3 -Wall -mfpu=vfp -mfloat-abi=hard --static -fPIC" CGO_LDFLAGS="$(OPAL_KELLY_FRONTPANEL_DIR)/third-party" GOPATH=$(@D)

define SENSEDAQ_BUILD_CMDS
	cd $(@D)/src && $(TARGET_CXX) -O3 -Wall -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9 -mfpu=neon  -mfloat-abi=hard --static -fPIC -c libdaq.cpp
	cd $(@D)/src && $(TARGET_AR) rcs libdaq.a libdaq.o
	cd $(@D)/src && $(GO_TARGET_ENV) $(GOSENSE_GO_ENV) $(HOST_DIR)/bin/go build -o sense-daq -x 2>&1 | tee $(@D)/buildlog.txt
endef

define SENSEDAQ_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/src/sense-daq $(TARGET_DIR)/opt/sdf/sense-daq
endef

$(eval $(golang-package))
