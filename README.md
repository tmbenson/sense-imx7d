# Building the code

The top-level Makefile in `imx7d-pico/buildroot/Makefile` does the following:

- Unpack the vendored version of buildroot (2018.08) if needed. Buildroot is unpacked outside of the build directory so that its download directory can cache files between builds. The first build will download everything, but after that, you can `rm -rf sdf_build` and re-run make without downloading any packages.
- Create the build directory, `sdf_build`
- Run `make BR2_EXTERNAL=... -C ... O=... sdf_imx7dpico_defconfig` to configure the build. This command can be re-run via `make defconfig` if the buildroot external configuration has been modified.
- Run make in `sdf_build`

Either the top-level Makefile can be used, or the above can be run manually for a fully out-of-tree build.

# Booting the PICO-PI-MX7D

## Boot mode configuration

The boot pin header on the PICO-PI-MX7D is the 6x2 pin header nearest the large header. I have the following configuration to enable serial download boots:

J J N J J N<br>
N J J N J J

where J indicates jumpered and N indicated not-jumpered.

## Pushing bootloader via USB SDP

For some reason, `imx_usb` does not work out-of-the-box on the PICO-PI-MX7D. Instead, the buildroot external packages mfgtools-uuu (an mfgtools fork from https://github.com/NXPmicro/mfgtools/wiki). To push uboot to the PICO-PI-MX7D, run the following after building and connecting the PICO-PI-MX7D:

`sudo ./sdf-build/host/bin/uuu sdf-build/images/u-boot.imx`

Sample commands to load and run the zImage and dtb files via tftp are included in `imx7d-pico/buildroot/scripts`.

## Running PipeTest

With the Opal Kelly shuttle connected via USB and powered on, you can run the PipeTest application from the Opal Kelly FrontPanel SDK as follows:

`pipetest /opt/opal-kelly-bitfiles/XEM6010-LX45/pipetest.bit repeat inf bench`